#!/usr/bin/env ruby

require 'faraday'
require 'json'
require 'optimist'
require 'json'

defaults = {
    :config => '/etc/phpipam2iscdhcpd.conf',
    :destfile => '/etc/dhcp/dhcpstatic.conf',
    :apiurl => 'http://localhost/phpipam/api',
    :apiuser=> '',
    :apipass => '',
}

opts = Optimist::options do
  usage '[options]'
  opt :config, 'Config file path', :short => 'c', :type => :string
  opt :destfile, 'Destination file', :short => 't', :type => :string
  opt :apiurl, 'API URL', :short => 'a', :type => :string
  opt :apiuser, 'Database user', :short => 'u', :type => :string
  opt :apipass, 'Database password', :short => 'p', :type => :string
  opt :verbose, 'Verbose output', :short => 'v'
end

config_file = opts[:config]||defaults[:config]

config = nil
if File.exist?(config_file)
  begin
    config = JSON.parse(File.read(config_file))
  rescue => detail
    puts detail.backtrace
    puts %Q(Error reading config from file #{config_file})
    exit(1)
  end
end

if config.nil? or (config.class != Hash)
  config = Hash.new
else
  config = Hash[config.map { |(k, v)| [k.to_sym, v] }]
end

# Precedence (low to high): defaults, config file, command line
config = defaults.merge(config).merge(Hash(opts.select { |k, v| !v.nil? }))

conn = Faraday.new(:url=>config[:apiurl])
conn.basic_auth(config[:apiuser],config[:apipass])
auth = conn.post('user/')
auth = JSON.parse(auth.body)
conn.headers['token'] = auth['data']['token']

addrs = JSON.parse(conn.get('addresses/?filter_by=mac&filter_match=regex&filter_value=/./').body)

destfile = File.new(config[:destfile],'w')

addrs['data'].each {|addr|
  puts addr['hostname'] if config[:verbose]

  destfile.puts %Q(
host #{addr['hostname'].split('.')[0]} {
  hardware ethernet #{addr['mac']};
  fixed-address #{addr['hostname']};
})

}

destfile.close
