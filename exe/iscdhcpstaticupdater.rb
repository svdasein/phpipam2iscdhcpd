#!/usr/bin/env ruby

require 'sinatra'

set :bind, '0.0.0.0'

get '/updatedhcpd' do
	output = String.new
	output += Time.now.to_s
	output += "\n#{%x(phpipam2iscdhcpd.rb -v 2>&1)}"
	output += Time.now.to_s
	%Q(<pre>#{output}</pre>)
end

